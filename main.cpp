#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("AudioPlayer");
    a.setApplicationDisplayName("Audio Player by Marty");
    MainWindow w;
    w.show();

    return a.exec();
}
