#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QListWidgetItem>
#include <QtWidgets>
#include <QMediaPlayer>
#include <QtWinExtras>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void playFile();
    void stopFile();
    void pauseFile();
    void addFiles();
    void deleteFile();
    void setVolume(int value);
    void playSelectedFile(QListWidgetItem* selectedFile);
    void nextTrack();
    void previousTrack();

private slots:
    void updatePosition(qint64 position);
    void setPosition(int);
    void updateDuration(qint64 duration);
    void queueMode();
    void randomMode();

    void on_action_triggered();

private:
    Ui::MainWindow *ui;
    QMediaPlayer mediaPlayer;
    bool isRandomModeOn=false;
    int currentTrackNumber=0;

};

#endif // MAINWINDOW_H
