/*
 * Самый банальны аудиоплеер с двумя режимами
 * Является переделкой курсовой, сделанной на C# в 2013 году
 * Последние изменения 03.04.2015
 * */
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), mediaPlayer(0) //конструктор
{
    //
    ui->setupUi(this);
    //
    connect(ui->playButton,SIGNAL(clicked()),this,SLOT(playFile()));
    connect(ui->stopButton,SIGNAL(clicked()),this,SLOT(stopFile()));
    connect(ui->pauseButton,SIGNAL(clicked()),this,SLOT(pauseFile()));
    connect(ui->volumeSlider,SIGNAL(valueChanged(int)),this,SLOT(setVolume(int)));
    connect(ui->volumeSlider,SIGNAL(valueChanged(int)),ui->volumeLabel,SLOT(setNum(int)));
    connect(ui->addButton,SIGNAL(clicked()),this,SLOT(addFiles()));
    connect(ui->trackList,SIGNAL(itemDoubleClicked(QListWidgetItem*)),this,SLOT(playSelectedFile(QListWidgetItem*)));
    connect(ui->deleteButton,SIGNAL(clicked()),this,SLOT(deleteFile()));
    connect(ui->trackSlider,SIGNAL(valueChanged(int)),this,SLOT(setPosition(int)));
    connect(ui->nextButton,SIGNAL(clicked()),this,SLOT(nextTrack()));
    connect(ui->previousButton,SIGNAL(clicked()),this,SLOT(previousTrack()));
    connect(ui->queueRadioButton,SIGNAL(clicked()),this,SLOT(queueMode()));
    connect(ui->randomButton,SIGNAL(clicked()),this,SLOT(randomMode()));
    connect(ui->action,SIGNAL(triggered()),this,SLOT(on_action_triggered()));
    //
    connect(&mediaPlayer,SIGNAL(positionChanged(qint64)),this,SLOT(updatePosition(qint64)));
    connect(&mediaPlayer,SIGNAL(durationChanged(qint64)),this,SLOT(updateDuration(qint64)));

    //Рандомирование
    QTime someTime(4,8,15);
    qsrand(someTime.secsTo((QTime::currentTime())));
}

MainWindow::~MainWindow() //деструктор
{
    delete ui;
}

void MainWindow::playFile() //Воспроизвести
{
    if(mediaPlayer.mediaStatus()==QMediaPlayer::NoMedia) //Если файла нет, то открыть диалог
    {
        addFiles(); //...и добавить файлы
        if(ui->trackList->count()) //...если были добавлены файлы
        {
            if(!isRandomModeOn)//...и режим выбран "по порядку"
            {
                mediaPlayer.setMedia(QUrl::fromLocalFile(ui->trackList->item(0)->text())); //...проиграть первый файл по списку
                currentTrackNumber=0;
                mediaPlayer.play();
                setWindowTitle(ui->trackList->item(0)->text());
            }
            else //...если выбран режим "рандомно"
            {
                int randomNumberFromList=qrand()%ui->trackList->count(); //...получить рандомное число из количества треков
                mediaPlayer.setMedia(QUrl::fromLocalFile(ui->trackList->item(randomNumberFromList)->text())); //...и проиграть его
                currentTrackNumber=randomNumberFromList;
                mediaPlayer.play();
                setWindowTitle(ui->trackList->item(randomNumberFromList)->text());
            }
        }
    }
        else
    {
        mediaPlayer.play(); //...если есть, то проиграть
    }
}

void MainWindow::stopFile() //Стоп
{
    mediaPlayer.stop();
}

void MainWindow::pauseFile() //Пауза
{
    mediaPlayer.pause();
}

void MainWindow::addFiles() //Добавить
{
    const QStringList musicPaths=QStandardPaths::standardLocations(QStandardPaths::MusicLocation);
    const QStringList filePaths=QFileDialog::getOpenFileNames(this,tr("Открыть файл"),
                                                        musicPaths.isEmpty()?QDir::homePath():musicPaths.first(),
                                                        tr("MP3 файлы (*.mp3);;Все файлы(*.*)"));
    if(!filePaths.isEmpty()) //Если выбраны файлы
    {
        ui->trackList->addItems(filePaths); //...добавить эти файлы
    }

}

void MainWindow::deleteFile() //Удалить
{
    int itemsNumber=ui->trackList->count()-1;
    for(int i=itemsNumber;i>=0;i--)
    {
        QListWidgetItem *tempItem=ui->trackList->item(i);
        if(tempItem->isSelected())
            delete tempItem;
    }
}

void MainWindow::setVolume(int value) //установить громкость
{
    mediaPlayer.setVolume(value);
}

void MainWindow::updatePosition(qint64 position) //обновлять слайдер по ходу песни
{
    ui->trackSlider->setValue(position);
    QTime duration(0,position/60000,qRound((position%60000)/1000.0));
    ui->currentTimeLabel->setText(duration.toString("mm:ss"));
}

void MainWindow::setPosition(int position) //изменить текущее положение песни
{
    mediaPlayer.setPosition(position);
    updatePosition(position);
}

void MainWindow::updateDuration(qint64 duration) //обновление диапазона слайдера
{
    ui->trackSlider->setRange(0,duration);
    ui->trackSlider->setEnabled(duration>0);
    ui->trackSlider->setPageStep(duration/10);
    QTime tempDuration(0,duration/60000,qRound((duration%60000)/1000.0));
    ui->lengthLabel->setText(tempDuration.toString("mm:ss"));
}

void MainWindow::playSelectedFile(QListWidgetItem* selectedFile) //воспрозвести выбранный в таблице файл
{
    mediaPlayer.setMedia(QUrl::fromLocalFile(selectedFile->text()));
    int itemsNumber=ui->trackList->count()-1;
    for(int i=itemsNumber;i>=0;i--)
    {
        QListWidgetItem *tempItem=ui->trackList->item(i);
        if(tempItem->isSelected())
            currentTrackNumber=i;
    }
    mediaPlayer.play();
    setWindowTitle(selectedFile->text());
}

void MainWindow::queueMode() //режим по порядку
{
    isRandomModeOn=false;
}

void MainWindow::randomMode() //рандомный режим
{
    isRandomModeOn=true;
}

void MainWindow::nextTrack() //Следующий трек
{
    if(!isRandomModeOn)
    {
        currentTrackNumber++;
        if(currentTrackNumber>ui->trackList->count()-1)
            currentTrackNumber=0;
        mediaPlayer.setMedia(QUrl::fromLocalFile(ui->trackList->item(currentTrackNumber)->text()));
        mediaPlayer.play();
        setWindowTitle(ui->trackList->item(currentTrackNumber)->text());
    }
    else
    {
        int randomNumberFromList=qrand()%ui->trackList->count();
        mediaPlayer.setMedia(QUrl::fromLocalFile(ui->trackList->item(randomNumberFromList)->text()));
        currentTrackNumber=randomNumberFromList;
        mediaPlayer.play();
        setWindowTitle(ui->trackList->item(randomNumberFromList)->text());
    }
}

void MainWindow::previousTrack() //Предыдущий трек
{
    if(!isRandomModeOn)
    {
        currentTrackNumber--;
        if(currentTrackNumber<0)
            currentTrackNumber=ui->trackList->count()-1;
        mediaPlayer.setMedia(QUrl::fromLocalFile(ui->trackList->item(currentTrackNumber)->text()));
        mediaPlayer.play();
        setWindowTitle(ui->trackList->item(currentTrackNumber)->text());
    }
    else
    {
        int randomNumberFromList=qrand()%ui->trackList->count();
        mediaPlayer.setMedia(QUrl::fromLocalFile(ui->trackList->item(randomNumberFromList)->text()));
        currentTrackNumber=randomNumberFromList;
        mediaPlayer.play();
        setWindowTitle(ui->trackList->item(randomNumberFromList)->text());
    }
}

void MainWindow::on_action_triggered()
{
    QMessageBox about;
    about.setText("Переделанный курсовой проект по программированию. Изначально сделан на C# в 2013 году. Переделан под Qt в 2015 году.");
    about.exec();
}
